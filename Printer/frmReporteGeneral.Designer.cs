﻿namespace Printer
{
    partial class frmReporteGeneral
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmReporteGeneral));
            this.dgvVentaLibre = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbtipopago = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lbtotalventas = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbcantidadventa = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbdescuentoreporte = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVentaLibre)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvVentaLibre
            // 
            this.dgvVentaLibre.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVentaLibre.Location = new System.Drawing.Point(-4, -1);
            this.dgvVentaLibre.Name = "dgvVentaLibre";
            this.dgvVentaLibre.Size = new System.Drawing.Size(1051, 399);
            this.dgvVentaLibre.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbdescuentoreporte);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.lbtipopago);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.lbtotalventas);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.lbcantidadventa);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 426);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1206, 150);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Resultados";
            // 
            // lbtipopago
            // 
            this.lbtipopago.AutoSize = true;
            this.lbtipopago.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbtipopago.Location = new System.Drawing.Point(117, 69);
            this.lbtipopago.Name = "lbtipopago";
            this.lbtipopago.Size = new System.Drawing.Size(440, 24);
            this.lbtipopago.TabIndex = 5;
            this.lbtipopago.Text = "Efectivo (250000),Tarjeta(120000),Codensa(300000)";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(30, 69);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 20);
            this.label5.TabIndex = 4;
            this.label5.Text = "Tipo pago:";
            // 
            // lbtotalventas
            // 
            this.lbtotalventas.AutoSize = true;
            this.lbtotalventas.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbtotalventas.Location = new System.Drawing.Point(271, 31);
            this.lbtotalventas.Name = "lbtotalventas";
            this.lbtotalventas.Size = new System.Drawing.Size(80, 24);
            this.lbtotalventas.TabIndex = 3;
            this.lbtotalventas.Text = "2500000";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(163, 33);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Total ventas: $";
            // 
            // lbcantidadventa
            // 
            this.lbcantidadventa.AutoSize = true;
            this.lbcantidadventa.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbcantidadventa.Location = new System.Drawing.Point(115, 33);
            this.lbcantidadventa.Name = "lbcantidadventa";
            this.lbcantidadventa.Size = new System.Drawing.Size(16, 17);
            this.lbcantidadventa.TabIndex = 1;
            this.lbcantidadventa.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(36, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Cantidad:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(13, 113);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 20);
            this.label2.TabIndex = 6;
            this.label2.Text = "Descuento:$";
            // 
            // lbdescuentoreporte
            // 
            this.lbdescuentoreporte.AutoSize = true;
            this.lbdescuentoreporte.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbdescuentoreporte.Location = new System.Drawing.Point(110, 111);
            this.lbdescuentoreporte.Name = "lbdescuentoreporte";
            this.lbdescuentoreporte.Size = new System.Drawing.Size(20, 24);
            this.lbdescuentoreporte.TabIndex = 7;
            this.lbdescuentoreporte.Text = "0";
            // 
            // frmReporteGeneral
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1040, 583);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dgvVentaLibre);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmReporteGeneral";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Reporte General";
            this.Load += new System.EventHandler(this.frmReporteGeneral_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvVentaLibre)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvVentaLibre;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lbtipopago;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lbtotalventas;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbcantidadventa;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbdescuentoreporte;
        private System.Windows.Forms.Label label2;
    }
}