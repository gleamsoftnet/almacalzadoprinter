﻿using Newtonsoft.Json;
using Printer.ApiService;
using Printer.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Printer
{
    public partial class frmReporteVendedores : Form
    {
        private apiRest api;
        public frmReporteVendedores()
        {
            InitializeComponent();
            api = new apiRest();
        }

        private void frmReporteVendedores_Load(object sender, EventArgs e)
        {
            var rest = api.GetReleases(UtilApi.apiUrl + UtilApi.ventaLibre);

            List<VentaLibre> deserializedProduct = JsonConvert.DeserializeObject<List<VentaLibre>>(rest);
          List<VentasAsesores> ventasAsesores=
                deserializedProduct.GroupBy(x => x.Vendedor)
              .Select(cl=> new VentasAsesores
                    {
                        VendedorId = cl.Key,
                        Cantidad = cl.Count(),
                        Ventas = string.Format("{0:0,0}", cl.Sum(c => c.Precio)),
                    }).ToList<VentasAsesores>();

            BindingSource binding = new BindingSource();
            binding.DataSource = ventasAsesores;
            dgvasesores.DataSource = binding;
        }
    }
}
