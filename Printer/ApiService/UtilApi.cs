﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 

namespace Printer.ApiService
{
    class UtilApi
    {
        public static string apiUrl = "https://almacalzado.azurewebsites.net/api/";
        public static string login = "account/login";
        public static string ventaLibre = "ApiVentaLibre";
        public static string ApiDevolucionVentaLibres = "ApiDevolucionVentaLibres";
        public static string ApiZapatoReferencia = "apizapatos?referencia=";
    }
}
