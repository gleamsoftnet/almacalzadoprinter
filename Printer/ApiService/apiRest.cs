﻿using Newtonsoft.Json;
using Printer.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text; 

namespace Printer.ApiService
{
    class apiRest
    {
        public string GetReleases(string url)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);

            request.Method = "GET";
            
            request.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            var content = string.Empty;

            using (var response = (HttpWebResponse)request.GetResponse())
            {
                using (var stream = response.GetResponseStream())
                {
                    using (var sr = new StreamReader(stream))
                    {
                        content = sr.ReadToEnd();
                    }
                }
            }

            return content;
        }
        public string ValidateLogin(LoginViewModel parameter)
        {
            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(UtilApi.apiUrl + UtilApi.login);
            request.Method = "POST";
            request.KeepAlive = true;
            request.ContentType = "application/json; charset=utf-8";
            // write the data to the request stream         
            using (StreamWriter writer = new StreamWriter(request.GetRequestStream()))
            {
                var PostData = JsonConvert.SerializeObject(parameter);

                writer.Write(PostData);
            }

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            string myResponse = "";
            using (System.IO.StreamReader sr = new System.IO.StreamReader(response.GetResponseStream()))
            {
                myResponse = sr.ReadToEnd();
            }
            return myResponse;
        }
        public string SendData(VentaLibre parameter)
        {
            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(UtilApi.apiUrl + UtilApi.ventaLibre);
            request.Method = "POST";
            request.KeepAlive = true;
            request.ContentType = "application/json; charset=utf-8";
            // write the data to the request stream         
            using (StreamWriter writer = new StreamWriter(request.GetRequestStream()))
            {
                var PostData = JsonConvert.SerializeObject(parameter);

                writer.Write(PostData);
            }

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            string myResponse = "";
            using (System.IO.StreamReader sr = new System.IO.StreamReader(response.GetResponseStream()))
            {
                myResponse = sr.ReadToEnd();
            }
            return myResponse;
        }
        public string SendDataDevolucion(DevolucionVentaLibre parameter)
        {
            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(UtilApi.apiUrl + UtilApi.ApiDevolucionVentaLibres);
            request.Method = "POST";
            request.KeepAlive = true;
            request.ContentType = "application/json; charset=utf-8";
            // write the data to the request stream         
            using (StreamWriter writer = new StreamWriter(request.GetRequestStream()))
            {
                var PostData = JsonConvert.SerializeObject(parameter);

                writer.Write(PostData);
            }

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            string myResponse = "";
            using (System.IO.StreamReader sr = new System.IO.StreamReader(response.GetResponseStream()))
            {
                myResponse = sr.ReadToEnd();
            }
            return myResponse;
        }
    }
}
