﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Printer.Models
{
    class DevolucionesViewModel
    {
        public int Id { get; set; }
        public decimal Precio { get; set; }
        public decimal Diferencia { get; set; }
        public int Vendedor { get; set; }
        public DateTimeOffset Fecha { get; set; }
    }
}
