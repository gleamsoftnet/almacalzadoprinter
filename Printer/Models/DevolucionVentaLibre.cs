﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Printer.Models
{
   public class DevolucionVentaLibre
    {
        public int Id { get; set; }
        public decimal Diferencia { get; set; }
        public string UsuarioId { get; set; }
        public DateTimeOffset Fecha { get; set; }        
        public int VentaLibreId { get; set; }
         
    }
}
