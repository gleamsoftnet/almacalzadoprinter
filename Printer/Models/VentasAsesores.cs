﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Printer.Models
{
    class VentasAsesores
    {
        public int VendedorId { get; set; }         
        public string Ventas { get; set; }
        public int Cantidad { get; set; }
    }
}
