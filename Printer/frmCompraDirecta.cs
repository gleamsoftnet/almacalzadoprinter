﻿using Newtonsoft.Json;
using Printer.ApiService;
using Printer.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Printer
{
    public partial class frmcompradirecta : Form
    {
        private List<VentaLibre> ventaLibres;
        private ZapatoViewModel zapatoData;
        private apiRest api;
        private string factura = null;
        private bool facturaImpresa = false;
        public frmcompradirecta()
        {
            InitializeComponent();
            txtcodvendedor.Focus();
            zapatoData = new ZapatoViewModel();
            ventaLibres = new List<VentaLibre>();
            api = new apiRest();
            lbusuario.Text = Properties.Settings.Default.Usuario;
            lbnombre.Text = Properties.Settings.Default.Nombres;
            lbcorreo.Text = Properties.Settings.Default.Correo;
            rbafectivo.Checked = true;
            lbtitle.Text = "Producto".PadRight(10) + "Cantidad".PadRight(10) + "Precio";

        }

        private void btnadditem_Click(object sender, EventArgs e)
        {

            if (facturaImpresa)
            {
                listBox1.Items.Clear();
                facturaImpresa = false;
                lbtotal.Text = "0";
                txtdescuento.Text = "0";
                txtPrice.Text = "";
                txtcantidad.Text = "";
                factura = null;
            }
            if (String.IsNullOrEmpty(txtcodvendedor.Text))
            {
                MessageBox.Show("Código vendedor  requerido");
                txtcodvendedor.Focus();
                return;
            }
            if (String.IsNullOrEmpty(txtcantidad.Text))
            {
                MessageBox.Show("Cantidad requerida");
                txtcantidad.Focus();
                return;
            }
            if (String.IsNullOrEmpty(txtPrice.Text))
            {
                MessageBox.Show("Precio requerido");
                txtPrice.Focus();
                return;
            }
            if (String.IsNullOrEmpty(txtdescuento.Text))
            {
                MessageBox.Show("Descuento no puede estar vacío");
                txtPrice.Focus();
                return;
            }
            float total = float.Parse(lbtotal.Text);
            float precio = float.Parse(txtPrice.Text) * float.Parse(txtcantidad.Text);
            listBox1.Items.Add(txtName.Text.PadRight(10) + txtcantidad.Text.PadRight(5) + precio);
            float totalNuevo = total + precio;
            lbtotal.Text = totalNuevo + "";
            //init VentaLibre 
            if (factura == null)
            {
                factura = GetRandomAlphaNumeric();
            }
            var dataVentaLibre = new VentaLibre();
            dataVentaLibre.Factura = factura;
            dataVentaLibre.Nombre = txtName.Text;
            dataVentaLibre.Cantidad = int.Parse(txtcantidad.Text);
            dataVentaLibre.Precio = decimal.Parse(txtPrice.Text);

            dataVentaLibre.Descuento = decimal.Parse(txtdescuento.Text);
            dataVentaLibre.UsuarioId = Properties.Settings.Default.Id;
            dataVentaLibre.Fecha = DateTimeOffset.Now;
            dataVentaLibre.Vendedor = int.Parse(txtcodvendedor.Text);
            if (zapatoData != null)
            {
                dataVentaLibre.zapato = zapatoData;
            }
            if (rbafectivo.Checked)
            {
                dataVentaLibre.FormaPago = "Efectivo";
            }
            if (rbcodensa.Checked)
            {
                dataVentaLibre.FormaPago = "Codensa";
            }
            if (rbtarjeta.Checked)
            {
                dataVentaLibre.FormaPago = "Tarjeta";
            }
            if (rbbigpass.Checked)
            {
                dataVentaLibre.FormaPago = "Big Pass";
            }
            ventaLibres.Add(dataVentaLibre);
            //txtName.Text = "";
            lbvender.Text = "Muy bien! Uno más?";
            lbvender.ForeColor = System.Drawing.Color.Blue;
            txtPrice.Text = "";
            txtdescuento.Text = "0";
            txtreferenciaproducto.Text = "";
            txtcantidad.Text = "1";
            lbexistenciastock.Text = "0";
            zapatoData = null;
        }
        private static string GetRandomAlphaNumeric()
        {
            var random = new Random();
            var chars = "abcdefghijklmnopqrstuvwxyz0123456789";
            return new string(chars.Select(c => chars[random.Next(chars.Length)]).Take(8).ToArray());
        }
        private void btndeleteproducto_Click(object sender, EventArgs e)
        {
            try
            {
                int pos = listBox1.SelectedIndex;
                decimal totalActual = decimal.Parse(lbtotal.Text);
                VentaLibre venta = ventaLibres[pos];
                decimal totalNew = (totalActual - venta.Precio);
                lbtotal.Text = totalNew.ToString();
                listBox1.Items.RemoveAt(pos);
                ventaLibres.RemoveAt(pos);
                lbvender.Text = "Opps! Has eliminado un producto :(";
                lbvender.ForeColor = System.Drawing.Color.Red;
            }
            catch (Exception)
            {

                return;
            }
        }

        private void btnimprimir_Click(object sender, EventArgs e)
        {
            if (rbdividido.Checked)
            {
                float cash = 0;
                float dividido = 0;
                if (String.IsNullOrEmpty(txtCash.Text))
                {
                    MessageBox.Show("Debe espesificar el valor en efectivo!");
                    return;
                }
                if (String.IsNullOrEmpty(txtvalortarjeta.Text))
                {
                    MessageBox.Show("Debe espesificar el valor en tarjeta!");
                    return;
                }
            }
            PrintDialog printDialog = new PrintDialog();

            PrintDocument printDocument = new PrintDocument();

            printDialog.Document = printDocument; //add the document to the dialog box...        

            printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(CreateReceipt); //add an event handler that will do the printing

            //on a till you will not want to ask the user where to print but this is fine for the test envoironment.

            //DialogResult result = printDialog.ShowDialog();

            //if (result == DialogResult.OK)
            //{
            //    printDocument.Print();

            //}
            lbvender.Text = "Supér bien! Ya estoy generando la factura....";
            lbvender.ForeColor = System.Drawing.Color.Green;
            printDocument.Print();
            if (!facturaImpresa)
            {
                guardarVenta();
            }
            facturaImpresa = true;
            txtcodvendedor.Text = "";
        }



        public void CreateReceipt(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {

            float cash = 0;
            float dividido = 0;
            float descuento = 0;
            if (!String.IsNullOrEmpty(txtCash.Text))
            {
                cash = float.Parse(txtCash.Text);
            }
            if (!String.IsNullOrEmpty(txtvalortarjeta.Text))
            {
                dividido = float.Parse(txtvalortarjeta.Text);
            }
            if (!String.IsNullOrEmpty(txtdescuento.Text))
            {
                descuento = float.Parse(txtdescuento.Text);
            }

            float change = 0.00f;

            //this prints the reciept

            Graphics graphic = e.Graphics;

            Font font = new Font("Courier New", 8); //must use a mono spaced font as the spaces need to line up

            float fontHeight = font.GetHeight();

            int startX = 10;
            int startY = 10;
            int offset = 20;


            graphic.DrawString(" CALZADO", new Font("Courier New", 20), new SolidBrush(Color.Black), startX, startY);
            offset = offset + 10;
            graphic.DrawString(" PALPASO", new Font("Courier New", 20), new SolidBrush(Color.Black), startX, startY + offset);
            offset = offset + 30;
            graphic.DrawString("    NIT ".PadRight(5) + "86078372", font, new SolidBrush(Color.Black), startX, startY + offset);
            offset = offset + 15;
            graphic.DrawString("    Fact: ".PadRight(5) + factura, font, new SolidBrush(Color.Black), startX, startY + offset);
            offset = offset + 15;
            graphic.DrawString("  Calle 38 sur #86C-24", font, new SolidBrush(Color.Black), startX, startY + offset);
            offset = offset + 15;
            graphic.DrawString("30 DÍAS DE GARANTIA", new Font("Courier New", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + offset);
            string top = "Item".PadRight(5) + " Cantidad".PadRight(5) + " Precio";
            offset = offset + 20;
            var fechaCompra = DateTime.Now.ToString("MM/dd/yyyy h:mm tt");
            graphic.DrawString("  " + String.Format("{0:c}", fechaCompra), font, new SolidBrush(Color.Black), startX, startY + offset);
            offset = offset + 18;
            graphic.DrawString(top, font, new SolidBrush(Color.Black), startX, startY + offset);
            offset = offset + (int)fontHeight; //make the spacing consistent
            graphic.DrawString("------------------------------------", font, new SolidBrush(Color.Black), startX, startY + offset);
            offset = offset + (int)fontHeight + 5; //make the spacing consistent

            float totalprice = 0.00f;

            foreach (string item in listBox1.Items)
            {
                //create the string to print on the reciept
                string productDescription = item;
                string productTotal = item.Substring(item.Length - 6, 6);
                float productPrice = float.Parse(item.Substring(item.Length - 5, 5));

                //MessageBox.Show(item.Substring(item.Length - 5, 5) + "PROD TOTAL: " + productTotal);


                totalprice += productPrice;

                if (productDescription.Contains("  -"))
                {
                    string productLine = productDescription.Substring(0, 30);

                    graphic.DrawString(productLine, new Font("Courier New", 10, FontStyle.Italic), new SolidBrush(Color.Red), startX, startY + offset);

                    offset = offset + (int)fontHeight + 5; //make the spacing consistent
                }
                else
                {
                    string productLine = productDescription;

                    graphic.DrawString(productLine, font, new SolidBrush(Color.Black), startX, startY + offset);

                    offset = offset + (int)fontHeight + 5; //make the spacing consistent
                }

            }



            //when we have drawn all of the items add the total

            offset = offset + 5; //make some room so that the total stands out.

            graphic.DrawString("Total a pagar ".PadRight(5) + String.Format("{0:c}", totalprice), new Font("Courier New", 8, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + offset);
            offset = offset + 30; //make some room so that the total stands out.
            if (descuento > 0)
            {
                graphic.DrawString("Descuento ".PadRight(5) + String.Format("{0:c}", descuento), new Font("Courier New", 8, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + offset);
                offset = offset + 30;
            }
            if (rbafectivo.Checked)
            {
                change = (cash - totalprice);
                if (cash > 0)
                {
                    graphic.DrawString("Efectivo ".PadRight(5) + String.Format("{0:c}", cash), font, new SolidBrush(Color.Black), startX, startY + offset);
                    offset = offset + 15;
                    graphic.DrawString("Cambio ".PadRight(5) + String.Format("{0:c}", change), font, new SolidBrush(Color.Black), startX, startY + offset);
                    offset = offset + 30; //make some room so that the total stands out.
                }
            }
            if (rbtarjeta.Checked)
            {
                graphic.DrawString("Forma de pago: ".PadRight(5) + "Tarjeta", font, new SolidBrush(Color.Black), startX, startY + offset);
                offset = offset + 30;
            }
            if (rbcodensa.Checked)
            {
                graphic.DrawString("Forma de pago: ".PadRight(5) + "Codensa", font, new SolidBrush(Color.Black), startX, startY + offset);
                offset = offset + 30;
            }
            if (rbbigpass.Checked)
            {
                graphic.DrawString("Forma de pago: ".PadRight(5) + "Big Pass", font, new SolidBrush(Color.Black), startX, startY + offset);
                offset = offset + 30;
            }

            if (rbdividido.Checked)
            {
                cash = float.Parse(txtCash.Text);
                graphic.DrawString("Forma de pago: ".PadRight(5) + "Dividido", font, new SolidBrush(Color.Black), startX, startY + offset);
                offset = offset + 15;
                graphic.DrawString("Efectivo ".PadRight(5) + String.Format("{0:c}", cash), font, new SolidBrush(Color.Black), startX, startY + offset);
                offset = offset + 15;
                graphic.DrawString("Tarjeta ".PadRight(5) + String.Format("{0:c}", dividido), font, new SolidBrush(Color.Black), startX, startY + offset);
                offset = offset + 30;
            }

            graphic.DrawString("Gracias por preferirnos,", font, new SolidBrush(Color.Black), startX, startY + offset);
            offset = offset + 15;
            graphic.DrawString("¡por favor vuelve pronto!", font, new SolidBrush(Color.Black), startX, startY + offset);
            offset = offset + 50;
            graphic.DrawString("------------------------------------", font, new SolidBrush(Color.Black), startX, startY + offset);

        }
        private void guardarVenta()
        {
            try
            {
                decimal descuento = 0;
                if (!String.IsNullOrEmpty(txtdescuento.Text))
                {
                    descuento = decimal.Parse(txtdescuento.Text);
                }
                foreach (var item in ventaLibres)
                {
                    item.Estado = "Vendido";
                    item.Descuento = descuento;
                    item.SubTotal = item.Precio - descuento;
                    item.Total = decimal.Parse(lbtotal.Text);
                    api.SendData(item);
                }
                ventaLibres.Clear();
                lbvender.Text = "Genial! Hemos terminado. Sigamos vendiento :)";
                lbvender.ForeColor = System.Drawing.Color.DarkSeaGreen;

            }
            catch (Exception)
            {

                throw;
            }
        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtcantidad_KeyPress(object sender, KeyPressEventArgs e)
        {
            // Ignore all non-control and non-numeric key presses.
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
            //  to Call the implementation in the base TextBox class,
            // which raises the KeyPress event.
            base.OnKeyPress(e);
        }

        private void txtPrice_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
            //  to Call the implementation in the base TextBox class,
            // which raises the KeyPress event.
            base.OnKeyPress(e);
        }

        private void txtCash_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
            //  to Call the implementation in the base TextBox class,
            // which raises the KeyPress event.
            base.OnKeyPress(e);
        }

        private void rbafectivo_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rb = sender as RadioButton;
            if (rb != null)
            {
                if (rb.Checked)
                {
                    txtCash.Visible = true;
                    txtvalortarjeta.Visible = false;
                }
            }
        }

        private void rbtarjeta_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rb = sender as RadioButton;
            if (rb != null)
            {
                if (rb.Checked)
                {
                    txtCash.Visible = false;
                    txtvalortarjeta.Visible = false;
                }
            }
        }

        private void rbcodensa_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rb = sender as RadioButton;
            if (rb != null)
            {
                if (rb.Checked)
                {
                    txtCash.Visible = false;
                    txtvalortarjeta.Visible = false;
                }
            }
        }

        private void rbbigpass_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rb = sender as RadioButton;
            if (rb != null)
            {
                if (rb.Checked)
                {
                    txtCash.Visible = false;
                    txtvalortarjeta.Visible = false;
                }
            }
        }

        private void txtPrice_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void txtCash_KeyUp(object sender, KeyEventArgs e)
        {
            //if (!string.IsNullOrEmpty(txtCash.Text))
            //{
            //    System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("en-US");
            //    int valueBefore = Int32.Parse(txtCash.Text, System.Globalization.NumberStyles.AllowThousands);
            //    txtCash.Text = String.Format(culture, "{0:N0}", valueBefore);
            //    txtCash.Select(txtCash.Text.Length, 0);
            //}
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void txtdescuento_KeyUp(object sender, KeyEventArgs e)
        {
            //if (!string.IsNullOrEmpty(txtdescuento.Text))
            //{
            //    System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("en-US");
            //    int valueBefore = Int32.Parse(txtdescuento.Text, System.Globalization.NumberStyles.AllowThousands);
            //    txtdescuento.Text = String.Format(culture, "{0:N0}", valueBefore);
            //    txtdescuento.Select(txtdescuento.Text.Length, 0);
            //}
        }

        private void txtPrice_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void txtcantidad_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnadditem.PerformClick();
            }
        }

        private void txtdescuento_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnadditem.PerformClick();
            }
        }

        private void txtcodvendedor_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnadditem.PerformClick();
            }
        }

        private void txtCash_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnadditem.PerformClick();
            }
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void txtPrice_TextChanged(object sender, EventArgs e)
        {
            //try
            //{
            //    if (!string.IsNullOrEmpty(txtPrice.Text))
            //    {

            //        System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("en-US");
            //        int valueBefore = int.Parse(txtPrice.Text, System.Globalization.NumberStyles.AllowThousands);
            //        txtPrice.Text = String.Format(culture, "{0:N0}", valueBefore);
            //        txtPrice.SelectgtxtPrice.Text.Length, 0);
            //    }
            //}
            //catch (Exception ex)
            //{

            //    MessageBox.Show("Error:" + ex.Message);
            //}

        }

        private void txtcodvendedor_TabIndexChanged(object sender, EventArgs e)
        {
            txtcantidad.Focus();
        }

        private void rbdividido_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rb = sender as RadioButton;
            if (rb != null)
            {
                if (rb.Checked)
                {
                    txtvalortarjeta.Visible = true;
                    txtCash.Visible = true;
                }
            }
        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void generalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmReporteGeneral fr = new frmReporteGeneral();
            fr.ShowDialog();

        }

        private void asesoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmReporteVendedores fr = new frmReporteVendedores();
            fr.ShowDialog();
        }

        private void consultarFacturaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmDialogFactura fr = new frmDialogFactura();
            fr.ShowDialog();
        }

        private void semanalToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void devolucionesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmDevoluciones fr = new frmDevoluciones();
            fr.ShowDialog();
        }

        private void txtdescuento_TextChanged(object sender, EventArgs e)
        {
            this.Text = txtdescuento.Text;

            lbdescuento.Text = txtdescuento.Text;
        }

        private void txtcodvendedor_Enter(object sender, EventArgs e)
        {

        }

        private void frmcompradirecta_KeyDown(object sender, KeyEventArgs e)
        {


        }

        private void txtcodvendedor_KeyDown_1(object sender, KeyEventArgs e)
        {

        }

        private void Btnconsultarreferencia_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtreferenciaproducto.Text))
            {
                MessageBox.Show("Referencia no puede estar vacío");
                txtreferenciaproducto.Focus();
                return;
            }
            var urlApi = UtilApi.apiUrl + UtilApi.ApiZapatoReferencia + txtreferenciaproducto.Text;
            var rest = api.GetReleases(urlApi);

            List<ZapatoViewModel> json = JsonConvert.DeserializeObject<List<ZapatoViewModel>>(rest);
            if (json.Count > 0)
            {
                int stock = json[0].Cantidad;
                if (stock > 0)
                {
                    txtName.Text = json[0].Nombre;
                    txtPrice.Text = json[0].PrecioVenta.ToString();
                    lbexistenciastock.Text = json[0].Cantidad.ToString();
                    zapatoData = json[0];
                }
                else
                {
                    MessageBox.Show("Referencia agotada!");
                    txtreferenciaproducto.Text = "";
                    txtreferenciaproducto.Focus();
                    return;
                }

            }
            else
            {
                MessageBox.Show("Referencia no encontrada");
                txtreferenciaproducto.Text = "";
                txtreferenciaproducto.Focus();
                return;
            }

        }
    }
}
