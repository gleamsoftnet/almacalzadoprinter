﻿using Newtonsoft.Json;
using Printer.ApiService;
using Printer.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Printer
{
    public partial class frmDialogFactura : Form
    {
        private List<VentaLibre> deserializedProduct;
        private apiRest api;
        public frmDialogFactura()
        {
            InitializeComponent();
            api = new apiRest();
            txtfacturaconsultar.Focus();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtfacturaconsultar.Text))
            {
                MessageBox.Show("Factura  requerida");
                txtfacturaconsultar.Focus();
                return;
            }
            var rest = api.GetReleases(UtilApi.apiUrl + UtilApi.ventaLibre+"?factura="+txtfacturaconsultar.Text);

             deserializedProduct = JsonConvert.DeserializeObject<List<VentaLibre>>(rest);
            if (deserializedProduct.Count()==0)
            {
                MessageBox.Show("Factura  no encontrada");
            }
            BindingSource binding = new BindingSource();
            binding.DataSource = deserializedProduct;
            dgvfactura.DataSource = binding;
        }

        private void button1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
            //  to Call the implementation in the base TextBox class,
            // which raises the KeyPress event.
            base.OnKeyPress(e);
        }

        private void dgvfactura_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int post = e.RowIndex;
            VentaLibre ventaLibres = deserializedProduct[post];
            txtvalorventa.Text = String.Format("{0:c}", ventaLibres.Precio);
            txtidventa.Text = ventaLibres.Id.ToString();
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtdiferencia.Text))
            {
                MessageBox.Show("Valor diferencia requerido!");
                txtdiferencia.Focus();
                return;
            }
            if (String.IsNullOrEmpty(txtidventa.Text))
            {
                MessageBox.Show("Debe seleccionar una venta!");
                txtidventa.Focus();
                return;
            }
            var param = new DevolucionVentaLibre();
            param.UsuarioId = Properties.Settings.Default.Id;
            decimal diferencia = decimal.Parse(txtdiferencia.Text);
            param.Diferencia = diferencia;
            param.VentaLibreId = int.Parse(txtidventa.Text);
            param.Fecha = DateTimeOffset.Now;
            api.SendDataDevolucion(param);
            MessageBox.Show("Devolución realizada correctamente!");
            txtvalorventa.Text = "0";
            txtidventa.Text = "";
            txtdiferencia.Text = "0";
        }
    }
}
