﻿using Newtonsoft.Json;
using Printer.ApiService;
using Printer.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;  
using System.Windows.Forms;

namespace Printer
{
    public partial class frmLogin : Form
    {
        private apiRest api = new apiRest();
        public frmLogin()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void bntingresar_Click(object sender, EventArgs e)
        {
            if (txtusuario.Text.Trim() != string.Empty || txtcontrasena.Text.Trim() != string.Empty)
            {
                var carDynamic = new LoginViewModel();
                carDynamic.Usuario = txtusuario.Text.Trim().ToString();
                carDynamic.Contrasena = txtcontrasena.Text.Trim().ToString();
                
                var response = api.ValidateLogin(carDynamic);
                if (response == "null")
                {
                    MessageBox.Show("Acceso negado. Ingresa por favor una cuenta de usuario válida.",
                    "Error de acceso",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation,
                    MessageBoxDefaultButton.Button1);
                }
                else
                {

                    var json = JsonConvert.DeserializeObject<UserInfo>(response);
                    Properties.Settings.Default.Id = json.Id;
                    Properties.Settings.Default.Usuario = json.Usuario;
                    Properties.Settings.Default.Correo = json.Correo;
                    Properties.Settings.Default.Nombres = json.Nombres;
                    Properties.Settings.Default.Estado = json.Estado;
                    Properties.Settings.Default.Fecha = json.FechaRegistro.ToString();
                    Properties.Settings.Default.Rol = json.Rol;
                    Properties.Settings.Default.Save();
                    MessageBox.Show("Bienvenid@ " + json.Nombres);
                    frmcompradirecta fr = new frmcompradirecta();
                    fr.Show();
                    Hide();
                }
            }
            else
            {
                MessageBox.Show("Campos obligatorios",
                    "Debe ingresar su usuario y contraseña",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error,
                    MessageBoxDefaultButton.Button1);
            }
        }

        private void txtcontrasena_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtcontrasena_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                bntingresar.PerformClick();
            }
        }

        private void txtusuario_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtcontrasena.Focus();
            }
        }
    }
}
