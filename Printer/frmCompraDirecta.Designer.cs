﻿namespace Printer
{
    partial class frmcompradirecta
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmcompradirecta));
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCash = new System.Windows.Forms.TextBox();
            this.btnadditem = new System.Windows.Forms.Button();
            this.btndeleteproducto = new System.Windows.Forms.Button();
            this.btnimprimir = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtvalortarjeta = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.rbdividido = new System.Windows.Forms.RadioButton();
            this.rbbigpass = new System.Windows.Forms.RadioButton();
            this.rbcodensa = new System.Windows.Forms.RadioButton();
            this.rbtarjeta = new System.Windows.Forms.RadioButton();
            this.rbafectivo = new System.Windows.Forms.RadioButton();
            this.lbtitle = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lbtotal = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lbusuario = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lbnombre = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lbcorreo = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lbvender = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.generalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asesoresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarFacturaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.devolucionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.semanalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mensualToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ventasCanceladasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txtcodvendedor = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtdescuento = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtcantidad = new System.Windows.Forms.TextBox();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.lbdescuento = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtreferenciaproducto = new System.Windows.Forms.TextBox();
            this.btnconsultarreferencia = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.lbexistenciastock = new System.Windows.Forms.Label();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // listBox1
            // 
            resources.ApplyResources(this.listBox1, "listBox1");
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Name = "listBox1";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // txtCash
            // 
            resources.ApplyResources(this.txtCash, "txtCash");
            this.txtCash.Name = "txtCash";
            this.txtCash.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCash_KeyDown);
            // 
            // btnadditem
            // 
            resources.ApplyResources(this.btnadditem, "btnadditem");
            this.btnadditem.Name = "btnadditem";
            this.btnadditem.UseVisualStyleBackColor = true;
            this.btnadditem.Click += new System.EventHandler(this.btnadditem_Click);
            // 
            // btndeleteproducto
            // 
            resources.ApplyResources(this.btndeleteproducto, "btndeleteproducto");
            this.btndeleteproducto.Name = "btndeleteproducto";
            this.btndeleteproducto.UseVisualStyleBackColor = true;
            this.btndeleteproducto.Click += new System.EventHandler(this.btndeleteproducto_Click);
            // 
            // btnimprimir
            // 
            resources.ApplyResources(this.btnimprimir, "btnimprimir");
            this.btnimprimir.Name = "btnimprimir";
            this.btnimprimir.UseVisualStyleBackColor = true;
            this.btnimprimir.Click += new System.EventHandler(this.btnimprimir_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtvalortarjeta);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.rbdividido);
            this.groupBox2.Controls.Add(this.rbbigpass);
            this.groupBox2.Controls.Add(this.rbcodensa);
            this.groupBox2.Controls.Add(this.rbtarjeta);
            this.groupBox2.Controls.Add(this.rbafectivo);
            this.groupBox2.Controls.Add(this.txtCash);
            this.groupBox2.Controls.Add(this.label2);
            resources.ApplyResources(this.groupBox2, "groupBox2");
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.TabStop = false;
            // 
            // txtvalortarjeta
            // 
            resources.ApplyResources(this.txtvalortarjeta, "txtvalortarjeta");
            this.txtvalortarjeta.Name = "txtvalortarjeta";
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.Name = "label12";
            this.label12.Click += new System.EventHandler(this.label12_Click);
            // 
            // rbdividido
            // 
            resources.ApplyResources(this.rbdividido, "rbdividido");
            this.rbdividido.Name = "rbdividido";
            this.rbdividido.TabStop = true;
            this.rbdividido.UseVisualStyleBackColor = true;
            this.rbdividido.CheckedChanged += new System.EventHandler(this.rbdividido_CheckedChanged);
            // 
            // rbbigpass
            // 
            resources.ApplyResources(this.rbbigpass, "rbbigpass");
            this.rbbigpass.Name = "rbbigpass";
            this.rbbigpass.TabStop = true;
            this.rbbigpass.UseVisualStyleBackColor = true;
            this.rbbigpass.CheckedChanged += new System.EventHandler(this.rbbigpass_CheckedChanged);
            // 
            // rbcodensa
            // 
            resources.ApplyResources(this.rbcodensa, "rbcodensa");
            this.rbcodensa.Name = "rbcodensa";
            this.rbcodensa.TabStop = true;
            this.rbcodensa.UseVisualStyleBackColor = true;
            this.rbcodensa.CheckedChanged += new System.EventHandler(this.rbcodensa_CheckedChanged);
            // 
            // rbtarjeta
            // 
            resources.ApplyResources(this.rbtarjeta, "rbtarjeta");
            this.rbtarjeta.Name = "rbtarjeta";
            this.rbtarjeta.TabStop = true;
            this.rbtarjeta.UseVisualStyleBackColor = true;
            this.rbtarjeta.CheckedChanged += new System.EventHandler(this.rbtarjeta_CheckedChanged);
            // 
            // rbafectivo
            // 
            resources.ApplyResources(this.rbafectivo, "rbafectivo");
            this.rbafectivo.Name = "rbafectivo";
            this.rbafectivo.TabStop = true;
            this.rbafectivo.UseVisualStyleBackColor = true;
            this.rbafectivo.CheckedChanged += new System.EventHandler(this.rbafectivo_CheckedChanged);
            // 
            // lbtitle
            // 
            resources.ApplyResources(this.lbtitle, "lbtitle");
            this.lbtitle.BackColor = System.Drawing.SystemColors.ControlDark;
            this.lbtitle.ForeColor = System.Drawing.Color.White;
            this.lbtitle.Name = "lbtitle";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // lbtotal
            // 
            resources.ApplyResources(this.lbtotal, "lbtotal");
            this.lbtotal.Name = "lbtotal";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.BackColor = System.Drawing.SystemColors.ControlDark;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Name = "label6";
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // lbusuario
            // 
            resources.ApplyResources(this.lbusuario, "lbusuario");
            this.lbusuario.ForeColor = System.Drawing.Color.White;
            this.lbusuario.Name = "lbusuario";
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            // 
            // lbnombre
            // 
            resources.ApplyResources(this.lbnombre, "lbnombre");
            this.lbnombre.ForeColor = System.Drawing.Color.White;
            this.lbnombre.Name = "lbnombre";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lbcorreo);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.lbusuario);
            this.groupBox3.Controls.Add(this.lbnombre);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label8);
            resources.ApplyResources(this.groupBox3, "groupBox3");
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.TabStop = false;
            // 
            // lbcorreo
            // 
            resources.ApplyResources(this.lbcorreo, "lbcorreo");
            this.lbcorreo.ForeColor = System.Drawing.Color.White;
            this.lbcorreo.Name = "lbcorreo";
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            // 
            // lbvender
            // 
            resources.ApplyResources(this.lbvender, "lbvender");
            this.lbvender.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbvender.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.lbvender.Name = "lbvender";
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.generalToolStripMenuItem,
            this.asesoresToolStripMenuItem,
            this.consultarFacturaToolStripMenuItem,
            this.reportesToolStripMenuItem});
            resources.ApplyResources(this.menuStrip1, "menuStrip1");
            this.menuStrip1.Name = "menuStrip1";
            // 
            // generalToolStripMenuItem
            // 
            this.generalToolStripMenuItem.Name = "generalToolStripMenuItem";
            resources.ApplyResources(this.generalToolStripMenuItem, "generalToolStripMenuItem");
            this.generalToolStripMenuItem.Click += new System.EventHandler(this.generalToolStripMenuItem_Click);
            // 
            // asesoresToolStripMenuItem
            // 
            this.asesoresToolStripMenuItem.Name = "asesoresToolStripMenuItem";
            resources.ApplyResources(this.asesoresToolStripMenuItem, "asesoresToolStripMenuItem");
            this.asesoresToolStripMenuItem.Click += new System.EventHandler(this.asesoresToolStripMenuItem_Click);
            // 
            // consultarFacturaToolStripMenuItem
            // 
            this.consultarFacturaToolStripMenuItem.Name = "consultarFacturaToolStripMenuItem";
            resources.ApplyResources(this.consultarFacturaToolStripMenuItem, "consultarFacturaToolStripMenuItem");
            this.consultarFacturaToolStripMenuItem.Click += new System.EventHandler(this.consultarFacturaToolStripMenuItem_Click);
            // 
            // reportesToolStripMenuItem
            // 
            this.reportesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.devolucionesToolStripMenuItem,
            this.semanalToolStripMenuItem,
            this.mensualToolStripMenuItem,
            this.ventasCanceladasToolStripMenuItem});
            this.reportesToolStripMenuItem.Name = "reportesToolStripMenuItem";
            resources.ApplyResources(this.reportesToolStripMenuItem, "reportesToolStripMenuItem");
            // 
            // devolucionesToolStripMenuItem
            // 
            this.devolucionesToolStripMenuItem.Name = "devolucionesToolStripMenuItem";
            resources.ApplyResources(this.devolucionesToolStripMenuItem, "devolucionesToolStripMenuItem");
            this.devolucionesToolStripMenuItem.Click += new System.EventHandler(this.devolucionesToolStripMenuItem_Click);
            // 
            // semanalToolStripMenuItem
            // 
            this.semanalToolStripMenuItem.Name = "semanalToolStripMenuItem";
            resources.ApplyResources(this.semanalToolStripMenuItem, "semanalToolStripMenuItem");
            this.semanalToolStripMenuItem.Click += new System.EventHandler(this.semanalToolStripMenuItem_Click);
            // 
            // mensualToolStripMenuItem
            // 
            this.mensualToolStripMenuItem.Name = "mensualToolStripMenuItem";
            resources.ApplyResources(this.mensualToolStripMenuItem, "mensualToolStripMenuItem");
            // 
            // ventasCanceladasToolStripMenuItem
            // 
            this.ventasCanceladasToolStripMenuItem.Name = "ventasCanceladasToolStripMenuItem";
            resources.ApplyResources(this.ventasCanceladasToolStripMenuItem, "ventasCanceladasToolStripMenuItem");
            // 
            // txtcodvendedor
            // 
            resources.ApplyResources(this.txtcodvendedor, "txtcodvendedor");
            this.txtcodvendedor.Name = "txtcodvendedor";
            this.txtcodvendedor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtcodvendedor_KeyDown);
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.Name = "label11";
            // 
            // txtdescuento
            // 
            resources.ApplyResources(this.txtdescuento, "txtdescuento");
            this.txtdescuento.Name = "txtdescuento";
            this.txtdescuento.TextChanged += new System.EventHandler(this.txtdescuento_TextChanged);
            this.txtdescuento.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPrice_KeyDown);
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.Name = "label10";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // txtcantidad
            // 
            resources.ApplyResources(this.txtcantidad, "txtcantidad");
            this.txtcantidad.Name = "txtcantidad";
            this.txtcantidad.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtcantidad_KeyDown);
            // 
            // txtPrice
            // 
            resources.ApplyResources(this.txtPrice, "txtPrice");
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPrice_KeyDown);
            // 
            // txtName
            // 
            resources.ApplyResources(this.txtName, "txtName");
            this.txtName.Name = "txtName";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtdescuento);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.txtcodvendedor);
            this.groupBox1.Controls.Add(this.txtName);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.txtPrice);
            this.groupBox1.Controls.Add(this.txtcantidad);
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            // 
            // label13
            // 
            resources.ApplyResources(this.label13, "label13");
            this.label13.Name = "label13";
            // 
            // lbdescuento
            // 
            resources.ApplyResources(this.lbdescuento, "lbdescuento");
            this.lbdescuento.Name = "lbdescuento";
            // 
            // label14
            // 
            resources.ApplyResources(this.label14, "label14");
            this.label14.Name = "label14";
            // 
            // txtreferenciaproducto
            // 
            resources.ApplyResources(this.txtreferenciaproducto, "txtreferenciaproducto");
            this.txtreferenciaproducto.Name = "txtreferenciaproducto";
            // 
            // btnconsultarreferencia
            // 
            resources.ApplyResources(this.btnconsultarreferencia, "btnconsultarreferencia");
            this.btnconsultarreferencia.Name = "btnconsultarreferencia";
            this.btnconsultarreferencia.UseVisualStyleBackColor = true;
            this.btnconsultarreferencia.Click += new System.EventHandler(this.Btnconsultarreferencia_Click);
            // 
            // label15
            // 
            resources.ApplyResources(this.label15, "label15");
            this.label15.Name = "label15";
            // 
            // lbexistenciastock
            // 
            resources.ApplyResources(this.lbexistenciastock, "lbexistenciastock");
            this.lbexistenciastock.Name = "lbexistenciastock";
            // 
            // frmcompradirecta
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.Controls.Add(this.lbexistenciastock);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.btnconsultarreferencia);
            this.Controls.Add(this.txtreferenciaproducto);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.lbdescuento);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lbtitle);
            this.Controls.Add(this.lbvender);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lbtotal);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnimprimir);
            this.Controls.Add(this.btndeleteproducto);
            this.Controls.Add(this.btnadditem);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.menuStrip1);
            this.ForeColor = System.Drawing.SystemColors.Desktop;
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmcompradirecta";
            this.TransparencyKey = System.Drawing.Color.Transparent;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCash;
        private System.Windows.Forms.Button btnadditem;
        private System.Windows.Forms.Button btndeleteproducto;
        private System.Windows.Forms.Button btnimprimir;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rbbigpass;
        private System.Windows.Forms.RadioButton rbcodensa;
        private System.Windows.Forms.RadioButton rbtarjeta;
        private System.Windows.Forms.RadioButton rbafectivo;
        private System.Windows.Forms.Label lbtitle;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lbtotal;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lbusuario;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lbnombre;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label lbcorreo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lbvender;
        private System.Windows.Forms.RadioButton rbdividido;
        private System.Windows.Forms.TextBox txtvalortarjeta;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem generalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem asesoresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem semanalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mensualToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarFacturaToolStripMenuItem;
        private System.Windows.Forms.TextBox txtcodvendedor;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtdescuento;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtcantidad;
        private System.Windows.Forms.TextBox txtPrice;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ToolStripMenuItem devolucionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ventasCanceladasToolStripMenuItem;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lbdescuento;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtreferenciaproducto;
        private System.Windows.Forms.Button btnconsultarreferencia;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lbexistenciastock;
    }
}

