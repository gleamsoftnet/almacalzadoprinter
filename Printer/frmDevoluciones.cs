﻿using Newtonsoft.Json;
using Printer.ApiService;
using Printer.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Printer
{
    public partial class frmDevoluciones : Form
    {
        private apiRest api;
        public frmDevoluciones()
        {
            InitializeComponent();
            api = new apiRest();
        }

        private void frmDevoluciones_Load(object sender, EventArgs e)
        {
            var rest = api.GetReleases(UtilApi.apiUrl + UtilApi.ApiDevolucionVentaLibres);

            List<DevolucionesViewModel> deserializedProduct = JsonConvert.DeserializeObject<List<DevolucionesViewModel>>(rest);

            BindingSource binding = new BindingSource();
            binding.DataSource = deserializedProduct;
            dgvdevoluciones.DataSource = binding;
        }
    }
}
