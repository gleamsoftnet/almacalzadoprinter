﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Printer.ApiService;
using Printer.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Printer
{
   
    public partial class frmReporteGeneral : Form
    {
        private apiRest api;
        public frmReporteGeneral()
        {
            InitializeComponent();
            api = new apiRest();
        }

        private void frmReporteGeneral_Load(object sender, EventArgs e)
        {
            var rest = api.GetReleases(UtilApi.apiUrl + UtilApi.ventaLibre);

            List<VentaLibre> deserializedProduct = JsonConvert.DeserializeObject<List<VentaLibre>>(rest);

            BindingSource binding = new BindingSource();
            binding.DataSource = deserializedProduct;
            dgvVentaLibre.DataSource = binding;
            decimal total = 0;
            int cantidad = 0;
            decimal efectivo = 0;
            decimal tarjeta = 0;
            decimal codensa = 0;
            decimal bigpass = 0;
            decimal dividido = 0;
            decimal descuento = 0;
           
            foreach (var item in deserializedProduct)
            {
                total += item.Precio;
                cantidad += item.Cantidad;
                
                 
                if (item.FormaPago.Equals("Efectivo"))
                {
                    efectivo += item.Precio;
                }
                if (item.FormaPago.Equals("Tarjeta"))
                {
                    tarjeta += item.Precio;
                }
                if (item.FormaPago.Equals("Codensa"))
                {
                    codensa += item.Precio;
                }
                if (item.FormaPago.Equals("Big Pass"))
                {
                    bigpass += item.Precio;
                }
                if (item.FormaPago.Equals("Dividido"))
                {
                    dividido += item.Precio;
                }
            }

            //var result = deserializedProduct
            //    .GroupBy(x => x.Factura,
            //(key, values) => new {
            //    Currency = key,
            //    Total = values.Sum(x => x.Descuento > 0 ? x.Descuento : x.Descuento)
            //}).ToList();
            //foreach(var dto in result)
            //{
            //    descuento += dto.Total;
            //}

            lbcantidadventa.Text = cantidad.ToString();
            lbtotalventas.Text = string.Format("{0:0,0}", total);
            lbtipopago.Text = "Efectivo ( " + string.Format("{0:0,0}", efectivo) + " ), " +
                " Tarjeta ($ " + string.Format("{0:0,0}", tarjeta) +
                " ) , Codensa ($ " + string.Format("{0:0,0}", codensa) +
                " ), Big Pass ($ " + string.Format("{0:0,0}", bigpass) +
                " ), Dividido ($ " + string.Format("{0:0,0}", dividido) + " )";
            lbdescuentoreporte.Text = string.Format("{0:0,0}", descuento);
        }
    }
}
